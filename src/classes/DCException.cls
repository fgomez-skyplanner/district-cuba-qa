/**
 * @description       : Exception Class
 * @author            : Ibrahim Napoles
 * @group             : 
 * @last modified on  : 08-24-2020
 * @last modified by  : Ibrahim Napoles
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   08-21-2020   Ibrahim Napoles   Initial Version
**/
public without sharing class DCException extends Exception {
    // Exception types
    enum ExceptionType {General, Permission}

    ExceptionType type;
    String msg;
    String customMsg;

    /********************** CONSTRUCTORS **********************/

    /** 
        Constructor 1
    */
    public DCException(ExceptionType type, String msg) {
        this.type = type;
        this.msg = msg;
        buildCustomMsg();

        // Set custom message
        setMessage(customMsg);
    }

    /********************** PRIVATE METHODS **********************/

    /**
        Build a custom exception message
    */
    private void buildCustomMsg() {
        customMsg = 
            type.name() + ' Exception | ' + msg;
    }

    @TestVisible
    private static Boolean isDCException(Exception ex) {
        System.debug('---->>>>>>>> ' +  ex.getTypeName());
        return ex.getTypeName() == 'DCException';
    }   

    /********************** PUBLIC METHODS **********************/

    /**
        Throw a 'General' exception
    */
    public static void throwException(String msg) {
        throw new DCException(ExceptionType.General, msg);
    }

    /**
        Throw a 'Permission' exception
    */
    public static void throwPermiException(String msg) {
        throw new DCException(ExceptionType.Permission, msg);
    }
}