/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 08-24-2020
 * @last modified by  : Ibrahim Napoles
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   08-24-2020   Ibrahim Napoles   Initial Version
**/
public with sharing class DCUtils {

    public static Map<Id, String> recordtypemap {get; set;}

    /**
    * @description Get Record Type list from a Object name
    * @author Ibrahim Napoles | 08-24-2020 
    * @param String objectName 
    * @return List<String> 
    **/
    public static List<String> fetchRecordTypeValues(String objectName){
        List<Schema.RecordTypeInfo> recordtypes = Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfos();
        recordtypemap = new Map<Id, String>();
        for(RecordTypeInfo rt : recordtypes) {
            if(rt.getName() != 'Master' && rt.getName().trim() != '' & rt.isAvailable()) {
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
            }
        }
        return recordtypemap.values();
    }

    /**
    * @description Get Record Type Id by a object name and Record Type name
    * @author Ibrahim Napoles | 08-24-2020 
    * @param String objName 
    * @param String recTypeName 
    * @return Id 
    **/
    public static Id getRecTypeId(String objName, String recTypeName) {
        return ((SObject)Type.forName(objName).newInstance())
            .getSObjectType()
            .getDescribe()
            .getRecordTypeInfosByName()
            .get(recTypeName)
            .getRecordTypeId();
    }
    
    /**
    * @description Get Record Type Id by a object name and Record Type developer name
    * @author Ibrahim Napoles | 08-24-2020 
    * @param String objName 
    * @param String recTypeName 
    * @return Id 
    **/
    public static Id getRecTypeIdByDevName(String objName, String recTypeName) {
        return ((SObject)Type.forName(objName).newInstance())
            .getSObjectType()
            .getDescribe()
            .getRecordTypeInfosByDeveloperName()
            .get(recTypeName)
            .getRecordTypeId();
    }
}