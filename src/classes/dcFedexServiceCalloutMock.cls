/**
 * @File Name          : dcFedexServiceCalloutMock.cls
 * @Description        : Mock class to make Fedex Web Service Callout Tests
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 22/7/2020 12:22:46 a. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
public with sharing class dcFedexServiceCalloutMock implements WebServiceMock {
    private String severity;

    public dcFedexServiceCalloutMock(String severity){
        this.severity = severity;
    }

    public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
         // start - specify the response you want to send
         dcFedexServiceClient.ProcessShipmentReply response_x = TestDataFactory.generateTestResponse(severity);
         // end
         response.put('response_x', response_x); 
    }
}