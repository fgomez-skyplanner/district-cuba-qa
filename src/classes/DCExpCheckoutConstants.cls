/**
 * @description       : Class to define Express Checkout Constants.
 * @author            : 
 * @group             : 
 * @last modified on  : 10-06-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-06-2020   Silvia Velazquez   Initial Version
**/
public with sharing class DCExpCheckoutConstants {
    public static final String PRIMERA_VEZ = '1'; 
    public static final String RENOVACION = '2'; 
    public static final String PRORROGA = '3'; 
    public static final String PRORROGA_ESTANCIA = '4'; 
    public static final String VISA_TURISMO = '5'; 
    public static final String VISA_HE11 = '6'; 
    public static final String CIUDADANIA_CUBANA = '7'; 
    public static final String CERT_ORIGINALES = '8'; 
    public static final String LEGALIZACION = '9'; 
    public static final String PODER_NOTARIAL = '10';
    public static final String DC_AGENCY = 'District Cuba';
    public static final String PROCEDURE_STATUS = 'Preparacion';
    public static final String PROCEDURE_ORIGIN = 'Web';
    public static final String OPERATIONS_ACCOUNT = 'Operaciones';
    public static final String ESCROW_ACCOUNT = 'Escrow';

    
    public static Map<String,String> typesMap = new Map<String,String>{
                        PRIMERA_VEZ => 'Pasaporte 1ra Vez',
                        RENOVACION => 'Renovacion de Pasaporte',
                        PRORROGA => 'Prorroga',
                        PRORROGA_ESTANCIA => 'Prorroga de Estancia',
                        VISA_TURISMO => 'Visa de Turismo',
                        VISA_HE11 => 'Visa HE-11',
                        CIUDADANIA_CUBANA => 'Ciudadania Cubana',
                        CERT_ORIGINALES => 'Certificados Originales',
                        LEGALIZACION => 'Legalizacion',
                        PODER_NOTARIAL => 'Poder Notarial'
    };

}