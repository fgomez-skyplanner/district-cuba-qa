/**
 * @File Name          : dcProcedureDefaultValues.cls
 * @Description        : controller class for dc_createProcedureWithDefaultValues Component 
 * @Author             : Elizabeth Betancourt
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 10-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/7/2020   Elizabeth Betancourt     Initial Version
 * 1.1    08-28-2020  Ibrahim Napoles          Escape query string.
**/
public with sharing class dcProcedureDefaultValues {
   
    @AuraEnabled
    public static dcDefaultValue getDefaults(String contactId, String procConfigId, String contactFields, String configFields) {
        
        //check accesibility
        checkAccessible(contactFields, configFields);
        
        string query = generateQuery(contactFields, 'Contact', contactId);
        List<Contact> contactList = Database.query(query);
        System.Debug(contactList);
        Map<string,string> dvMap1 = new Map<string,string>();
        if (contactList.size() > 0 ){
            dvMap1 = putValuestoDefault(contactFields, contactList[0]);
        }
        query = generateQuery(configFields, 'Procedure_Setup__c', procConfigId);
        System.Debug(query);
        List<Procedure_Setup__c> configList = Database.query(query);
        System.Debug(configList);
        Map<string,string> dvMap2 = new Map<string,string>();
        if (configList.size() > 0 ){
            dvMap2 = putValuestoDefault(configFields, configList[0]);
        }
        Map<string,string> dvMap = new  Map<string,string>();
        
        dvMap.putAll(dvMap1);
        dvMap.putAll(dvMap2); 
        
        System.Debug(dvMap);
        
        return new dcDefaultValue(dvMap);
    }
    
    private static String generateQuery(string selectfields, string fromObject, string objId){
        string whereStr = ' FROM ' + string.escapeSingleQuotes(fromObject) + ' WHERE Id = \'' + string.escapeSingleQuotes(objId) + '\'';
        string queryStr ='SELECT ' + string.escapeSingleQuotes(selectfields) +  whereStr ;
        System.Debug(objId);
        System.Debug(whereStr);
        System.Debug(queryStr);
        return queryStr;
    }
    
    private static Map<string,string> putValuestoDefault(string fields, sObject obj){
        Map<string,string> dvMap = new Map<string,string>();
        
        Map<string, Object> dvMapObject = obj.getPopulatedFieldsAsMap();
        
        List<String> strList = fields.split(',');
        
        String value = '';
        for (string key: strList){
            string key1 = 'inmix__'+key;
            value = dvMapObject.containsKey(key) ?  String.valueOf(dvMapObject.get(key)) : '';
            if (value=='') 
                value = dvMapObject.containsKey(key1) ?  String.valueOf(dvMapObject.get(key1)) : '';
            System.Debug(value);
            System.Debug(key+':'+value);
            if (value.contains('00:00:00'))
                  value = value.replace('00:00:00', '').trim();
                
            dvMap.put(key,value);   
            dvMap.put(key1,value);                         
        }
        return dvMap;
    }

    public static void checkAccessible(String contactFields, String configFields){

        String methodName='dcProcedureDefaultValues.checkAccessible';
        CRUDEnforce.dmlAccessible('Contact', putNamespace(contactFields), methodName);
        CRUDEnforce.dmlAccessible('inmix__Procedure_Setup__c', putNamespace(configFields), methodName);
    }

    private static List<String> putNamespace(String strFields){
        
        List<String> fields = strFields.split(strFields);
        List<String> withNamespaceList = new List<String>();
        
        String prefijo = 'inmix__';
        
        for(string f: fields){
            if (isCustomField(f)){ 
                    withNamespaceList.add(prefijo+f); 
            }
            else{
                withNamespaceList.add(f);  
            }
        }

        return withNamespaceList;
    }

    private static boolean isCustomField(String f){
        return f.endsWith('__c');
    }
}