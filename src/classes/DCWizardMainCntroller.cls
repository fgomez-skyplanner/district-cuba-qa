/**
 * @description       : 
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 10-15-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-08-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardMainCntroller {

	/**
	* @description 
	* @author Silvia Velazquez | 10-08-2020 
	* @param Id procedureId
	* @return Procedure__c 
	**/
	@RemoteAction
	global static Procedure__c getProcedure(Id procedureId) {	
		DCExpCheckoutProcedureManager manager = new DCExpCheckoutProcedureManager();
		return manager.getProcedure(procedureId);
	}
}