/**
 * @File Name          : dcFedExServiceInvocableTest.cls
 * @Description        : Test Class for Invocable Method dcFedExServiceInvocable.generateTracking
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/7/2020   Silvia Velazquez     Initial Version
**/

@isTest
private class dcFedExServiceInvocableTest {

    @isTest
	static void generateTestTrackingOK(){
        Map<String,String> params = TestDataFactory.getTramiteParams(true,true,true);
        dcFedExServiceInvocable.InputParam inputParam = new dcFedExServiceInvocable.InputParam();
        inputParam.tramiteId = params.get('tramiteId');
        inputParam.configurationId = params.get('configurationId');
        List<dcFedExServiceInvocable.InputParam> listParams = new List<dcFedExServiceInvocable.InputParam>{inputParam};
        List<String> result = new List<String>();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('SUCCESS')); //generating a success response
        result = dcFedExServiceInvocable.generateTracking(listParams);
        Test.stopTest();
        System.assertEquals(true, String.isNotBlank(result[0]), '');
    }

    @isTest
	static void generateTestTrackingInvalidId(){
        Map<String,String> params = TestDataFactory.getTramiteParams(false,true,true);
        dcFedExServiceInvocable.InputParam inputParam = new dcFedExServiceInvocable.InputParam();
        inputParam.tramiteId = params.get('tramiteId');
        inputParam.configurationId = params.get('configurationId');
        List<dcFedExServiceInvocable.InputParam> listParams = new List<dcFedExServiceInvocable.InputParam>{inputParam};
        List<String> result = new List<String>();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error response
        result = dcFedExServiceInvocable.generateTracking(listParams);
        Test.stopTest();
        System.assertEquals(null, result[0], '');
    } 
    
    @isTest
	static void generateTestTrackingInvalidPack(){
        Map<String,String> params = TestDataFactory.getTramiteParams(true,false,true);
        dcFedExServiceInvocable.InputParam inputParam = new dcFedExServiceInvocable.InputParam();
        inputParam.tramiteId = params.get('tramiteId');
        inputParam.configurationId = params.get('configurationId');
        List<dcFedExServiceInvocable.InputParam> listParams = new List<dcFedExServiceInvocable.InputParam>{inputParam};
        List<String> result = new List<String>();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error response
        result = dcFedExServiceInvocable.generateTracking(listParams);
        Test.stopTest();
        System.assertEquals(null, result[0], '');
    }   

}