/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-19-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-13-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardInformationController {
    public static final String BIRTH_PROVINCE_LABEL = 'Provincia de nacimiento en Cuba';
    public static final String BIRTH_CITY_LABEL = 'Municipio de nacimiento en Cuba';
    public static final String BIRTH_COUNTRY_LABEL = 'País de origen si nació fuera de Cuba';
    public static final String REF_PROVINCE_LABEL = 'Provincia de la persona de referencia*';
    public static final String REF_CITY_LABEL = 'Municipio de la persona de referencia*';
    public static final String LAST_PROVINCE_LABEL = 'Mi última dirección: Provincia*';
    public static final String PENULT_PROVINCE_LABEL = 'Penúltima dirección: Provincia';
    public static final String PENULT_CITY_LABEL = 'Penúltima dirección: Municipio';
    public static final String MARITAL_STATUS_LABEL = 'Estado Civil*';
    public static final String GENDER_LABEL = 'Sexo*';
    public static final String EYES_COLOR_LABEL = 'Color de ojos*';
    public static final String SKIN_COLOR_LABEL = 'Color de piel*';
    public static final String HAIR_COLOR_LABEL = 'Color de pelo*';
    public static final String MIGRATION_STATUS_LABEL = 'Clasificación Migratoria al salir de Cuba*';
    public static final String EDUCATION_LABEL = 'Nivel de escolaridad';

    public Date currentDay {get; set;}

    public DCWizardInformationController(){
        currentDay = Date.today();
    }

    @RemoteAction
    global static InformationWrapper getPicklistValues(){
        InformationWrapper result = new InformationWrapper();
       
        result.birthProvinces = new List<String>{BIRTH_PROVINCE_LABEL};
        result.refProvinces = new List<String>{REF_PROVINCE_LABEL};
        result.lastProvinces = new List<String>{LAST_PROVINCE_LABEL};
        result.penultProvinces = new List<String>{PENULT_PROVINCE_LABEL};

		for(Schema.PicklistEntry pickListVal : Procedure__c.Birth_Province__c.getDescribe().getPicklistValues()){
            result.birthProvinces.add(pickListVal.getLabel());
            result.refProvinces.add(pickListVal.getLabel());
            result.lastProvinces.add(pickListVal.getLabel());
            result.penultProvinces.add(pickListVal.getLabel());
        }

        result.birthCountries = new List<String>();
        result.birthCountries.add(BIRTH_COUNTRY_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Birth_Country__c.getDescribe().getPicklistValues()){
            result.birthCountries.add(pickListVal.getLabel());
        }

        result.maritalStatus = new List<String>();
        result.maritalStatus.add(MARITAL_STATUS_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Marital_Status__c.getDescribe().getPicklistValues()){
            result.maritalStatus.add(pickListVal.getLabel());
        }

        result.genders = new List<String>();
        result.genders.add(GENDER_LABEL);
        for(Schema.PicklistEntry pickListVal : Procedure__c.Gender__c.getDescribe().getPicklistValues()){
            result.genders.add(pickListVal.getLabel());
        }
        
        result.eyesColor = new List<String>();
        result.eyesColor.add(EYES_COLOR_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Eyes_Color__c.getDescribe().getPicklistValues()){
            result.eyesColor.add(pickListVal.getLabel());
        }

        result.skinColor = new List<String>();
        result.skinColor.add(SKIN_COLOR_LABEL);
        for(Schema.PicklistEntry pickListVal : Procedure__c.Skin_Color__c.getDescribe().getPicklistValues()){
            result.skinColor.add(pickListVal.getLabel());
        }

        result.hairColor = new List<String>();
        result.hairColor.add(HAIR_COLOR_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Hair_Color__c.getDescribe().getPicklistValues()){
            result.hairColor.add(pickListVal.getLabel());
        }

        result.migrationStatus = new List<String>();
        result.migrationStatus.add(MIGRATION_STATUS_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Immigration_Status__c.getDescribe().getPicklistValues()){
            result.migrationStatus.add(pickListVal.getLabel());
        }

        result.educationalLevel = new List<String>();
        result.educationalLevel.add(EDUCATION_LABEL);	
        for(Schema.PicklistEntry pickListVal : Procedure__c.Nivel_de_Escolaridad__c.getDescribe().getPicklistValues()){
            result.educationalLevel.add(pickListVal.getLabel());
        }

        result.dependentMunicipalities = getDependentPicklistValues();
        
        return result;
    }


    @RemoteAction
    global static Procedure__c generateProcedure(Procedure__c procedure, ValuesToConvertWrapper values){                                           
            DCExpCheckoutProcedureManager manager = new DCExpCheckoutProcedureManager();
            validateProcedure(procedure);
            completeProcedureData(procedure, values);
            System.debug('Procedure Received: '+ procedure);
            return manager.upsertProcedure(procedure, values.procedureParam);
    }

    
    global static List<ProvincesMapWrapper> getDependentPicklistValues(){
        Map<Object,List<String>> dependentPicklistValues = new Map<Object,List<String>>();
        //Get dependent field result
        Schema.DescribeFieldResult dependentFieldResult = Procedure__c.Birth_Municipality_City__c.getDescribe();
        //Get dependent field controlling field 
        Schema.sObjectField controllerField = dependentFieldResult.getController();
        //Check controlling field is not null
        if(controllerField == null){
            return null;
        } 
        //Get controlling field result
        Schema.DescribeFieldResult controllerFieldResult = controllerField.getDescribe();
        //Get controlling field picklist values if controlling field is not a checkbox
        List<Schema.PicklistEntry> controllerValues = (controllerFieldResult.getType() == Schema.DisplayType.Boolean ? null : controllerFieldResult.getPicklistValues());
        
        //It is used to decode the characters of the validFor fields. 
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        
        for (Schema.PicklistEntry entry : dependentFieldResult.getPicklistValues()){
            if (entry.isActive()){
            //The PicklistEntry is serialized and deserialized using the Apex JSON class and it will check to have a 'validFor' field
                List<String> base64chars = String.valueOf(((Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                for (Integer i = 0; i < controllerValues.size(); i++){
                    Object controllerValue = (controllerValues == null ? (Object) (i == 1) : (Object) (controllerValues[i].isActive() ? controllerValues[i].getLabel() : null));
                    Integer bitIndex = i / 6;
                    Integer bitShift = 5 - Math.mod(i, 6 );
                    if(controllerValue == null || (base64map.indexOf(base64chars[bitIndex]) & (1 << bitShift)) == 0){
                        continue;
                    } 
                    if(!dependentPicklistValues.containsKey(controllerValue)){
                        dependentPicklistValues.put(controllerValue, new List<String>());
                    }
                    dependentPicklistValues.get(controllerValue).add(entry.getLabel());
                }
            }
        }
        List<ProvincesMapWrapper> result = new List<ProvincesMapWrapper>();
        
        for(Object key: dependentPicklistValues.keySet()){
            ProvincesMapWrapper item = new ProvincesMapWrapper();
            item.province = key;
            item.municipalities = dependentPicklistValues.get(key);
            result.add(item);
        }
        return result;
    }

    global static void validateProcedure(Procedure__c procedure){

        if(procedure.Birth_Province__c == BIRTH_PROVINCE_LABEL){
            procedure.Birth_Province__c = null;
        }

        if(procedure.Birth_Municipality_City__c == BIRTH_CITY_LABEL){
            procedure.Birth_Municipality_City__c = null;
        }

        if(procedure.Birth_Country__c == BIRTH_COUNTRY_LABEL){
            procedure.Birth_Country__c = 'Cuba';
        }

        if(procedure.Nivel_de_Escolaridad__c == EDUCATION_LABEL){
            procedure.Nivel_de_Escolaridad__c = null;
        }

        if(procedure.References_Province__c == REF_PROVINCE_LABEL){
            procedure.References_Province__c = null;
        }

        if(procedure.References_Municipality__c == REF_CITY_LABEL){
            procedure.References_Municipality__c = null;
        }

        if(procedure.Residence_Address_2_Province__c == PENULT_PROVINCE_LABEL){
            procedure.Residence_Address_2_Province__c = null;
        }

        if(procedure.Residence_Address_2_City__c == PENULT_CITY_LABEL){
            procedure.Residence_Address_2_City__c = null;
        }
    }

    global static void completeProcedureData(Procedure__c procedure, ValuesToConvertWrapper values){
        procedure.Phone__c = String.valueOf(values.Phone);
        procedure.Reference_Phone__c = String.valueOf(values.Reference_Phone);
        procedure.Residence_Year_1_From__c = String.valueOf(values.Residence_Year_1_From);
        procedure.Residence_Year_1_To__c = String.valueOf(values.Residence_Year_1_To);
        procedure.Residence_Year_2_From__c = (values.Residence_Year_2_From !=null) ? String.valueOf(values.Residence_Year_2_From) : null;
        procedure.Residence_Year_2_To__c = (values.Residence_Year_2_To != null) ? String.valueOf(values.Residence_Year_2_To) : null;
        procedure.Birthday__c = Date.parse(values.Formatted_Birthday); 
        procedure.Departure_Date__c = Date.parse(values.Formatted_Departure_Date);       
    }

    global static Date convertToDate(String dateStr){
        //1988-01-01T00:00:00
        dateStr = dateStr.substringBefore('T');
        return Date.valueOf(dateStr);
    }

    global class InformationWrapper{
        public List<String> birthProvinces;
        public List<String> birthCountries;
        public List<String> refProvinces;
        public List<String> lastProvinces;
        public List<String> penultProvinces;
        public List<String> maritalStatus;
        public List<String> genders;
        public List<String> eyesColor;
        public List<String> skinColor;
        public List<String> hairColor;
        public List<String> migrationStatus;
        public List<String> educationalLevel;
        public List<ProvincesMapWrapper> dependentMunicipalities;
    }

    global class ProvincesMapWrapper{
        public Object province;
        public List<String> municipalities;
    }

    global class ValuesToConvertWrapper{
        public Long Phone;
        public Long Reference_Phone;
        public Integer Residence_Year_1_From;
        public Integer Residence_Year_1_To;
        public Integer Residence_Year_2_From;
        public Integer Residence_Year_2_To;
        public String Birthday;
        public String Formatted_Birthday;
        public String Departure_Date; 
        public String Formatted_Departure_Date; 
        public String procedureType;
        public String procedureParam;
    }
}