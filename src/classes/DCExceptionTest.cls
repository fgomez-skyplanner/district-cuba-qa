/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 08-24-2020
 * @last modified by  : Ibrahim Napoles
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   08-24-2020   Ibrahim Napoles   Initial Version
**/
@IsTest
private class DCExceptionTest {
    @IsTest
    static void throwPermiExceptionTest(){
        
        Test.startTest();
        try {
            DCException.throwPermiException('throwPermiExceptionTest');
        } catch (Exception ex) {
            DCException.isDCException(ex);
            System.assertNotEquals(null, ex, 'throwPermiExceptionTest');
                
        }
        Test.stopTest();
        
    }

    @IsTest
    static void throwExceptionTest(){
        
        Test.startTest();
        try {
            DCException.throwException('throwExceptionTest');
        } catch (Exception ex) {
            DCException.isDCException(ex);
            System.assertNotEquals(null, ex, 'throwExceptionTest');    
        }
        Test.stopTest();
        
    }
}