/**
 * @File Name          : AsyncDcFedexServiceCalloutMock.cls
 * @Description        : Class to generate fake response for asyncDcFedexServiceClient
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 24/7/2020 11:33:50 a. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
public with sharing class AsyncDcFedexServiceCalloutMock implements WebServiceMock{
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
        // start - specify the response you want to send
        //do nothing
    }
}