({
	invoke : function(component, event, helper) {     
              
        
        var idconfig = component.get("v.idconfig");
        var idclient = component.get("v.idclient");
        var contactFieldsList = component.get("v.contactFieldsList");
        var configFiledsList = "Sales_Price__c,ConsulateManifestPayment__c,ConsulatePayment__c,Agency_Commission__c,Agency__c";
        var params= { "contactId": idclient , "procConfigId": idconfig, "contactFields":contactFieldsList, "configFields":configFiledsList };
        
        return helper.runServerAction(component, helper, 'getDefaults', params).then(
			function(result) {
                
                if (result) {
                    //alert(JSON.stringify(result.defaultMapValues));
                    component.set("v.defaultValues", result.defaultMapValues);
                    
                    helper.createRecord(component, helper);                
                }
			}
        ).catch(function(error) {
            console.error('getDefaults -> ' + error);
        });
    }
})