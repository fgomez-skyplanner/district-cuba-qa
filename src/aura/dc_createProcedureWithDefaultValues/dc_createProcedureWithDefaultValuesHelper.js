({
	getDefaults : function(component) {
        
        var contactFieldsList=component.get("v.contactFieldsList");
        var prcedureFieldsList=component.get("v.prcedureFieldsList");
        
        var arrayFieldsFrom = contactFieldsList.split(",");
        var arrayFieldsTo = prcedureFieldsList.split(",");
        
        const defaultvalue = {};
        
        for( const key of arrayFieldsTo){
           
            var index = arrayFieldsTo.indexOf(key);
            var field = arrayFieldsFrom[index];
            
            defaultvalue[key] = this.getDefaultValue(component,field);
        }
        //llenar tipo de prooroga
        var specificProcedure = component.get("v.specificProcedure");      
        if (specificProcedure.includes("Prorroga") && !specificProcedure.includes("Estancia") ) { 
            var subType ="";
            if (specificProcedure.includes("Express")) 
                subType = "Express ";
            if (specificProcedure.includes("Doble"))   
                subType = subType + "Doble";
            else 
                subType = subType + "Regular";
            
            defaultvalue['Type__c']=subType;
        }
        var price = component.get("v.defaultValues.Sales_Price__c");
        var consulatePayment = component.get("v.defaultValues.ConsulatePayment__c");
        var commission = component.get("v.defaultValues.Agency_Commission__c");
        var consulateManifestPayment = component.get("v.defaultValues.ConsulateManifestPayment__c");
        var agency = component.get("v.defaultValues.Agency__c");
        
        var defaultName = component.get("v.defaultName");
        
        //alert(commission);
        defaultvalue['Name']=defaultName;
        defaultvalue['Subtotal__c']=price;
        defaultvalue['Total__c']=price;
        defaultvalue['Consulate_Payment__c']=consulatePayment; 
        defaultvalue['ConsulateManifestPayment__c']=consulateManifestPayment;
        defaultvalue['Agency_Commission__c']=commission;
        defaultvalue['Agency__c']=agency;
        
        return defaultvalue;
	},
    
    getDefaultValue: function(component, field){
              
            //set key for values lookup
            var key = 'v.defaultValues.'+field;
             
            //use the key to lookup value returned in LDS
            return component.get(key);             
    },
    
    createRecord: function(component, helper){
        
        var recTypeId = component.get("v.recordTypeIdProcedure");
        //var defaultvalue = {"Last_Name__c": "My Test"};
        var defaultvalue = this.getDefaults(component);
        //alert(defaultvalue["References_Province__c"]);
        //alert(JSON.stringify(defaultvalue));
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
		   "entityApiName": "Procedure__c",
           "recordTypeId": recTypeId, 
           "defaultFieldValues": defaultvalue
           
		});
		createRecordEvent.fire(); 
    },
    
    runServerAction : function(cmp, helper, method, params) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(self, function(response) {
				var state = response.getState();
                //alert(state);
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    }
})