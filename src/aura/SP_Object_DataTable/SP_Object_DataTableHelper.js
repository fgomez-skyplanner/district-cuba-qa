/**
 * @File Name          : SP_Object_DataTableHelper.js
 * @Description        :
 * @Author             : Ibrahim Napoles
 * @Group              :
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 12/19/2019, 9:58:31 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/6/2019   Ibrahim Napoles     Initial Version
 **/
({
  fireComponentEvent: function(component, action) {
    var myEvent = component.getEvent("searchEvent");
    myEvent.setParams({
      sortBy: component.get("v.sortBy"),
      sortDirection: component.get("v.sortDirection"),
      isSort: action == "isSort" ? true : false,
      isLoadMore: action == "isLoadMore" ? true : false
    });

    myEvent.fire();
  },
  fireDataTableEventAction: function(component, row, action) {
    var myEvent = component.getEvent("actionEvent");
    let paramRow = Object.assign({}, row); ;
      
    myEvent.setParams({
      row: paramRow
    });

    myEvent.fire();
  }
});