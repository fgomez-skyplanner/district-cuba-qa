<!-- 
Lightning Datepicker for AngularJs 1.X.X
@aurhor		Fernando Gomez
@date		5/24/2016
@version	1.0.0
-->
<apex:component layout="none">
    <apex:attribute name="placeholder"
        description="Placeholder value of input"
        required="false" type="String" default="..." />
    <apex:attribute name="ngModel"
        description="AngularJs ng-model expression"
        required="false" type="String" default="unk" />
    <apex:attribute name="ngChange" 
        description="AngularJs ng-changed expression"
        required="false" type="String" default="null" />
    <apex:attribute name="ngDisabled" 
        description="AngularJs ng-disabled expression"
        required="false" type="String" default="null" />
    <apex:attribute name="disabledWeekdays" 
        description="Weekdays that should appear as disabled"
        required="false" type="String" default="null" />
    <apex:attribute name="minDate" 
        description="Selectable days should be posterior to min date."
        required="false" type="String" default="null" />
            
    <div class="slds-form-element"
        xmlns="http://www.w3.org/2000/svg" 
        xmlns:xlink="http://www.w3.org/1999/xlink"
        sp-datepicker="true"
        ng-model="{! ngModel }"	
        ng-change="{! ngChange }"
        sp-disabled-weekdays="{! disabledWeekdays }"
        sp-min-date="{! minDate }">
        
        <div class="slds-form-element__control 
            slds-input-has-icon slds-input-has-icon--right">
            <svg aria-hidden="true" 
                ng-click="IF(OR(ISNULL(ngDisabled), !ngDisabled), 'open()', null)"
                class="slds-input__icon slds-icon-text-default">
                <use xlink:href="{! URLFOR($Resource.SalesforceLightningV202,
                    '/assets/icons/utility-sprite/svg/symbols.svg#event') }"></use>
            </svg>
            
            <input class="slds-input" 
                type="text" 
                aria-autocomplete="list" 
                role="combobox"
                aria-expanded="true" 
                ng-focus="open()"
                ng-disabled="{! IF(ISNULL(ngDisabled), 'false', ngDisabled) }"
                placeholder="{! placeholder }"
                ng-readonly="true"
                ng-value="{! ngModel } | date:'MMMM d, yyyy'" />
        </div>
            
        <div ng-if="show" class="slds-datepicker slds-dropdown slds-dropdown--left"
            sp-datepicker-stop-propagation-on="click">
            <div class="slds-datepicker__filter slds-grid">
                <div class="slds-shrink-none">
                    <div class="slds-select_container">
                        <select class="slds-select"
                            ng-model="currYear"
                            ng-change="toYear()"
                            ng-disabled="true"
                            ng-options="y as y for y in years">			            
                        </select>
                    </div>
                </div>
                <div class="slds-datepicker__filter--month slds-grid 
                        slds-grid--align-spread slds-grow">
                    <div class="slds-align-middle">
                        <button ng-click="lastMonth()"
                            class="slds-button slds-button--icon-container">
                            <svg aria-hidden="true" class="slds-button__icon 
                                slds-button__icon--small">
                                <use xlink:href="{! URLFOR($Resource.SalesforceLightningV202,
                                '/assets/icons/utility-sprite/svg/symbols.svg#left') }">
                                </use>
                            </svg>
                        </button>
                    </div>
                    <h2 ng-bind="currDate | date:'MMM'" class="slds-align-middle" 
                        aria-live="assertive" aria-atomic="true"></h2>
                    <div class="slds-align-middle">
                        <button ng-click="nextMonth()"
                            class="slds-button slds-button--icon-container">
                            <svg aria-hidden="true" class="slds-button__icon 
                                slds-button__icon--small">
                                <use xlink:href="{! URLFOR($Resource.SalesforceLightningV202,
                                '/assets/icons/utility-sprite/svg/symbols.svg#right') }">
                                </use>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="slds-shrink-none">
                    <div class="slds-align-middle">
                        <button ng-click="close()"
                            class="slds-button slds-button--icon-container">
                            <svg aria-hidden="true" class="slds-button__icon">
                                <use xlink:href="{! URLFOR($Resource.SalesforceLightningV202,
                                '/assets/icons/utility-sprite/svg/symbols.svg#close') }">
                                </use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <table class="datepicker__month" role="grid" aria-labelledby="month">
                <thead>
                    <tr>
                        <th scope="col">
                            <abbr title="Sunday">Sun</abbr>
                        </th>
                        <th scope="col">
                            <abbr title="Monday">Mon</abbr>
                        </th>
                        <th scope="col">
                            <abbr title="Tuesday">Tue</abbr>
                        </th>
                        <th scope="col">
                            <abbr title="Wednesday">Wed</abbr>
                        </th>
                        <th scope="col">
                            <abbr title="Thursday">Thu</abbr>
                        </th>
                        <th scope="col">
                            <abbr title="Friday">Fri</abbr>
                        </th>
                        <th scope="col">
                            <abbr title="Saturday">Sat</abbr>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="week in weeks">
                        <td ng-repeat="d in [0, 1, 2, 3, 4, 5, 6]"
                            ng-class="{
                                'slds-is-today': week[d].isToday,
                                'slds-disabled-text': 
                                    !week[d].isCurrent || week[d].isDisabled,
                                'slds-is-selected': week[d].isSelected
                            }"
                            role="gridcell" aria-disabled="true">
                            <span ng-click="select(week[d])"
                                class="slds-day" ng-bind="week[d].day"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" role="gridcell">
                            <!-- <a href="javascript:void(0)"
                                ng-click="today()"
                                class="slds-show--inline-block 
                                    slds-p-bottom--x-small">Today</a> -->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>		
</apex:component>